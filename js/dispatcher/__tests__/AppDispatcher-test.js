jest.dontMock('../AppDispatcher.js');

describe('AppDispatcher', function() {
    it('is an instance of the Flux Dispatcher', function() {
        const Dispatcher = require('flux').Dispatcher;
        const instance = require('../AppDispatcher');
        expect(instance).toEqual(jasmine.any(Dispatcher));
    });
});
